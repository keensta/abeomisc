package com.lrns123.abeomisc.runnables;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;


public class WarpRunnable implements Runnable {

	private Player player;
	private Location location;
	private String locName;
	
	public WarpRunnable(Player ply, Location loc, String locName)
	{
		this.player = ply;
		this.location = loc;
		this.locName = locName;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		player.teleport(location);
		player.sendMessage(ChatColor.BLUE + "Welcome to " + locName);
	}
}
