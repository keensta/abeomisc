package com.lrns123.abeomisc.listeners;

import java.util.HashSet;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.lrns123.abeomisc.AbeoMisc;

public class DragonEgg implements Listener{
	
	public HashSet<String> players = new HashSet<String>();
	
	public DragonEgg(AbeoMisc plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void entityDamage(EntityDamageEvent ev) {
		if(ev.getEntityType() == EntityType.ENDER_DRAGON) {
			
			EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) ev;
			
			if(event.getDamager() instanceof Player) {
				Player damager = (Player) event.getDamager();
				players.add(damager.getName());
			}
		}
	}
	
	@EventHandler
	public void playerInteract(PlayerInteractEvent ev) {
		Player ply = (Player) ev.getPlayer();
		Block cBlock;
		if(ev.getClickedBlock() != null) {
			cBlock = ev.getClickedBlock();
		} else {
			return;
		}
		
		if(cBlock.getType() == Material.DRAGON_EGG) {
			Random rm = new Random();
			int chancePick = rm.nextInt(100) +1;
			if(ply != null && chancePick == 100 && players.contains(ply.getName())) {
				cBlock.setType(Material.AIR);
				ply.getInventory().addItem(new ItemStack(Material.DRAGON_EGG));
			}
		}
	}

}
