package com.lrns123.abeomisc.listeners;

import org.bukkit.event.Listener;
import com.lrns123.abeomisc.AbeoMisc;

public class BlockDropListener implements Listener{
	
	public AbeoMisc plugin;
	
	public BlockDropListener(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

}
