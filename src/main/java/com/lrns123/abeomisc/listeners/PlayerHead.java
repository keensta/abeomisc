package com.lrns123.abeomisc.listeners;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.lrns123.abeomisc.AbeoMisc;

public class PlayerHead implements Listener {
	
	public AbeoMisc plugin;

	public PlayerHead (AbeoMisc plugin) {
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player ply = event.getEntity();
		Player killer = event.getEntity().getKiller();
		
		if(killer instanceof Player) {
			Random ran = new Random();
			if(ran.nextDouble() < 0.05) {
				event.getDrops().add(setSkin(ply.getName()));
			}
		}
	}
	
	public static ItemStack setSkin(String name) {
		ItemStack s = new ItemStack(Material.SKULL_ITEM);
		s.setDurability((short) 3);
		SkullMeta meta = (SkullMeta) s.getItemMeta();
		meta.setOwner(name);
		s.setItemMeta(meta);
		return s;
	}
	
}
