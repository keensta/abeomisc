package com.lrns123.abeomisc.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import com.lrns123.abeomisc.AbeoMisc;

public class BedrockListener implements Listener{
	
	public AbeoMisc plugin;
	
	
	public BedrockListener(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent ev) {
		Player ply = ev.getPlayer();
		if(ply.getInventory().contains(Material.BEDROCK)) {
			ply.getInventory().remove(Material.BEDROCK);
		}
		if(ply.getEnderChest().contains(Material.BEDROCK)) {
			ply.getEnderChest().remove(Material.BEDROCK);
		}
	}
	
	@EventHandler
	public void onChestOpen(InventoryOpenEvent ev) {
		if(ev.getInventory().contains(Material.BEDROCK)) {
			ev.getInventory().remove(Material.BEDROCK);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBedRockPlace(BlockPlaceEvent ev) {
		
		if(ev.isCancelled())
			return;
		
		if(ev.getBlock().getType() == Material.BEDROCK) {
			ev.setCancelled(true);
		}
		
	}
	
}
