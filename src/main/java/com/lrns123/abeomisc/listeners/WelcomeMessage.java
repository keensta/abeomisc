package com.lrns123.abeomisc.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.lrns123.abeomisc.AbeoMisc;

public class WelcomeMessage implements Listener{
	
	public WelcomeMessage(AbeoMisc plugin)
	{
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent ev)
	{
		ev.getPlayer().sendMessage(ChatColor.BLUE + "Welcome to Abeo");
	}
	
}
