package com.lrns123.abeomisc.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import com.lrns123.abeomisc.AbeoMisc;

public class PotionListener implements Listener{
	
	public AbeoMisc plugin;
	
	public PotionListener(AbeoMisc plugin) {
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPotionUse(BrewEvent ev) {
		if(ev.getContents().contains(Material.FERMENTED_SPIDER_EYE) &&
				ev.getContents().contains(new ItemStack(PotionEffectType.NIGHT_VISION.getId())) ) {
			ev.setCancelled(true);
			if(ev.getContents().getViewers() != null) {
				for(int i = 0; i < ev.getContents().getViewers().size(); i++) {
					Player ply = Bukkit.getPlayerExact(ev.getContents().getViewers().get(i).getName());
					ply.sendMessage(ChatColor.RED + "You may not brew an invisibility potion.");
				}
			}
			
		}
	}

}
