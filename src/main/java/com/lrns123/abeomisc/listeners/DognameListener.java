package com.lrns123.abeomisc.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.commands.NamePetCommand;

public class DognameListener implements Listener{
	
	private AbeoMisc plugin;
	
	public DognameListener(AbeoMisc plugin){
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	
	@EventHandler
	public void playerInteractEntity(PlayerInteractEntityEvent ev) {
		Player ply = ev.getPlayer();
		Entity ent = ev.getRightClicked();
		NamePetCommand namePet = plugin.getNamePetCommand();
		if(namePet.petNames.containsKey(ply.getName())) {
			if(ent.getType() == EntityType.WOLF) {
				Wolf wolf = (Wolf) ent;
				if(wolf.getOwner().equals(ply)) {	
					String name = namePet.petNames.get(ply.getName());
					if(name != null) {
						wolf.setCustomName(ChatColor.GRAY + name);
						wolf.setCustomNameVisible(true);
						wolf.getWorld().playEffect(wolf.getLocation(), Effect.MOBSPAWNER_FLAMES, 200);
						ply.playSound(wolf.getLocation(), Sound.WOLF_SHAKE, 15, 1);
						namePet.petNames.remove(ply.getName());
					}
					//quick fix to make wolves stop growling and aggressive always.
				} else {
					ply.sendMessage(ChatColor.RED + "You cannot name that wolf, it is not yours.");
					ply.sendMessage(ChatColor.GREEN + "This wolf belongs to " + ChatColor.GRAY + wolf.getOwner().getName());
				}
			} else 
			if(ent.getType() == EntityType.OCELOT) {
				Ocelot cat = (Ocelot) ent;
				if(cat.getOwner().equals(ply)) {	
					String name = namePet.petNames.get(ply.getName());
					if(name != null) {
						cat.setCustomName(ChatColor.GRAY + name);
						cat.setCustomNameVisible(true);
						cat.getWorld().playEffect(cat.getLocation(), Effect.MOBSPAWNER_FLAMES, 200);
						ply.playSound(cat.getLocation(), Sound.CAT_MEOW, 15, 1);
						namePet.petNames.remove(ply.getName());
					}
				} else {
					ply.sendMessage(ChatColor.RED + "You cannot name that cat, it is not yours.");
					ply.sendMessage(ChatColor.GREEN + "This cat belongs to " + ChatColor.GRAY + cat.getOwner().getName());
				}
			} else {
				ply.sendMessage(ChatColor.RED + "That creature is not a wolf, nor a cat.");
			}
		}
	}
	
	@EventHandler
	public void playerInteractBlock(PlayerInteractEvent ev) {
		if((ev.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (ev.getAction().equals(Action.RIGHT_CLICK_AIR))) {
			Player ply = ev.getPlayer();
			NamePetCommand namePet = plugin.getNamePetCommand();
			if(namePet.petNames.containsKey(ply.getName())) {
				ply.sendMessage(ChatColor.RED + "Naming of pet cancelled...");
				namePet.petNames.remove(ply.getName());
			}
		}
	}
	
	@EventHandler
	public void playerInteractWolfHappy(PlayerInteractEntityEvent ev) {
		Player ply = ev.getPlayer();
		Entity ent = ev.getRightClicked();
		if(ent instanceof Wolf) {
			Wolf wolf = (Wolf) ent;
			if(wolf.getOwner() == ply) {
				if(wolf.isAngry()) {
					wolf.setAngry(false);
				}
			} else {
				if(wolf.getOwner() != null) {
					ply.sendMessage(ChatColor.AQUA + "This wolf belongs to " + ChatColor.GRAY +  wolf.getOwner().getName());
				}
			}
		}else if(ent instanceof Ocelot) {
			Ocelot cat = (Ocelot) ent;
			if(cat.getOwner() != null) {
				ply.sendMessage(ChatColor.GREEN + "This cat belongs to " + ChatColor.GRAY + cat.getOwner().getName());
			}
		}
		
	}

}
