package com.lrns123.abeomisc.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.lrns123.abeomisc.AbeoMisc;

public class WatchListener implements Listener {

	public WatchListener(AbeoMisc plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev) {
		if ( (ev.getItem() != null && ev.getItem().getType() == Material.WATCH) && (ev.getAction() == Action.RIGHT_CLICK_AIR || ev.getAction() == Action.RIGHT_CLICK_BLOCK) ) {
			
			long time =  ev.getPlayer().getWorld().getTime();
			long hour = (time % 24000L / 1000L);
			long min = ((time - (hour * 1000)) * 3L) / 50L;
			String timeStr = ((hour + 6) % 24) + ":" + (min < 10 ? "0" + min : min);
			
			ev.getPlayer().sendMessage(ChatColor.BLUE + "Current time of day: " + ChatColor.WHITE + timeStr);
		}
	}
	
}
