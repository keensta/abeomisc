package com.lrns123.abeomisc.listeners;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.runnables.WarpRunnable;

public class VoidDamage implements Listener {

	public AbeoMisc plugin;

	public VoidDamage(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	private Vector getVector(FileConfiguration cfg, String path) {
		try {
			List<Integer> list = cfg.getIntegerList(path);
			if (list == null || list.size() != 3)
				return new Vector();

			return new Vector(list.get(0), list.get(1), list.get(2));
		} catch (Exception e) {
			return new Vector();
		}
	}
	
	@EventHandler
	public void VDamage(EntityDamageEvent ev) {
		Entity e = ev.getEntity();
		DamageCause dc = ev.getCause();
		if (e instanceof Player) {
			Player ply = (Player) ev.getEntity();
			if (dc == DamageCause.VOID) {
				FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));

				if (!cfg.contains("spawnpos") || !cfg.contains("spawnworld")) {
					ply.sendMessage(ChatColor.RED + "Something has gone wrong!!");
					return;
				}

				Vector pos = getVector(cfg, "spawnpos");
				String world = cfg.getString("spawnworld");
				if (ply.getHealth() < 10)
					ply.setHealth(10);

				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new WarpRunnable(ply, new Location(Bukkit.getWorld(world), pos.getX(), pos.getY(), pos.getZ()), "spawn"), 20);
				ply.sendMessage(ChatColor.BLUE + "You are being saved hold on!!");

				try {
					cfg.save(new File(plugin.getDataFolder(), "warp.yml"));
				} catch (IOException e1) {
					Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save warp.yml!");
					e1.printStackTrace();
				}
			}
		}
	}

}
