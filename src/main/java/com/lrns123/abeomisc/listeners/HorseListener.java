package com.lrns123.abeomisc.listeners;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;

import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.commands.HorseCommand;
import com.lrns123.abeomisc.util.Horse;

public class HorseListener implements Listener {

    private HashMap<String, Horse> horses = new HashMap<String, Horse>();
    private AbeoMisc plugin;

    public HorseListener(AbeoMisc plugin) {
        this.plugin = plugin;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onHorseTame(EntityTameEvent ev) {
        if(ev.getEntityType() == EntityType.HORSE) {
            org.bukkit.entity.Horse bHorse = (org.bukkit.entity.Horse) ev.getEntity();
            
            if(!horses.containsKey(bHorse.getUniqueId().toString())) {
                String owner = ((Player) ev.getOwner()).getName();
                Horse h = new Horse(bHorse, owner);

                horses.put(bHorse.getUniqueId().toString(), h);
                saveData(h);
                ((Player) ev.getOwner()).sendMessage(ChatColor.GREEN + "The horse has been tamed and protected");
            }
        }
    }

    @EventHandler
    public void onHorseInteract(PlayerInteractEntityEvent ev) {
        if(ev.getRightClicked().getType() == EntityType.HORSE) {
            org.bukkit.entity.Horse bHorse = (org.bukkit.entity.Horse) ev.getRightClicked();
            
            if(bHorse.isTamed() && !horses.containsKey(bHorse.getUniqueId().toString()) && bHorse.getOwner().equals(ev.getPlayer())) {
                String owner = bHorse.getOwner().getName();
                Horse h = new Horse(bHorse, owner);
                
                horses.put(bHorse.getUniqueId().toString(), h);
                saveData(h);
                ((Player) bHorse.getOwner()).sendMessage(ChatColor.GREEN + "The horse has been protected");
                ev.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onHorseDamage(EntityDamageByEntityEvent ev) {
        Entity damagedE = ev.getEntity();
        
        if(damagedE.getType() == EntityType.HORSE) {
            if(horses.containsKey(damagedE.getUniqueId().toString())) {
            	Horse h = horses.get(damagedE.getUniqueId().toString());
            	
            	if(ev.getDamager() instanceof Player) {
            		Player p = (Player) ev.getDamager();
                	
                	if(h.getOwner().equals(p.getName()))
                		return;
            	}
            	
                ev.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onHorseLeash(PlayerLeashEntityEvent ev) {
        Entity e = ev.getEntity();
        
        if(e.getType() == EntityType.HORSE) {
            org.bukkit.entity.Horse bHorse = (org.bukkit.entity.Horse) e;
            if(horses.containsKey(e.getUniqueId().toString())) {
                Horse h = horses.get(e.getUniqueId().toString());
                
                if(!h.getOwner().equals(ev.getPlayer().getName()) && !h.isRider(ev.getPlayer().getName())) {
                    ev.setCancelled(true);
                    ev.getPlayer().sendMessage(ChatColor.RED + "This isn't your horse and can't be leashed.");
                }
                
                h.updateInfo(bHorse);
                horses.put(h.getId().toString(), h);
                saveData(h);
            }
        }
    }
    
    @EventHandler
    public void onHorseMount(VehicleEnterEvent ev) {
        Entity e = ev.getVehicle();
        
        if(e.getType() == EntityType.HORSE) {
            org.bukkit.entity.Horse bHorse = (org.bukkit.entity.Horse) e;
            
            if(horses.containsKey(bHorse.getUniqueId().toString())) {
                    Player p = (Player) ev.getEntered();
                    Horse h = horses.get(e.getUniqueId().toString());
                    
                    if(!h.getOwner().equals(p.getName()) && !h.isRider(p.getName())) {
                        ev.setCancelled(true);
                        p.sendMessage(ChatColor.RED + "This isn't your horse.");
                    }
                    
                    h.updateInfo(bHorse);
                    horses.put(h.getId().toString(), h);
                    saveData(h);
            }
        }
    }

    @EventHandler
    public void interactWithHorse(PlayerInteractEntityEvent ev) {
    	Player p = ev.getPlayer();
    	Entity e = ev.getRightClicked();
    	
    	if(e.getType() == EntityType.HORSE) {
    		if(horses.containsKey(e.getUniqueId().toString())) {
    			HorseCommand hc = plugin.getHorseCommand();
    			
    			if(hc.players.containsKey(p.getName())) {
    				ev.setCancelled(true);
    				Horse h = horses.get(e.getUniqueId().toString());
    				
    				p.sendMessage(ChatColor.BLUE + "UUID: " + h.getId());
                    p.sendMessage(ChatColor.BLUE + "Owner: " + h.getOwner());
                    p.sendMessage(ChatColor.BLUE + "Health: " + renderBar(ChatColor.GRAY + "[", h.getCurrentHealth(), h.getMaxHealth()));
                    p.sendMessage(ChatColor.BLUE + "Age: " + h.getAge());
                    p.sendMessage(ChatColor.BLUE + "Tamed: " + h.isTamed() + " Saddled: " + h.isSaddled());
                    p.sendMessage(ChatColor.BLUE + "Colour: " + h.getColour() + ChatColor.BLUE + " Sytle: " + h.getStyle() + " Variant: " + h.getVariant());
                    
                    return;
    			}
    			
    			if(hc.modifyOwner.containsKey(p.getName())) {
    				ev.setCancelled(true);
    				
    				OfflinePlayer op = Bukkit.getOfflinePlayer(hc.modifyOwner.get(p.getName()));
    				Horse h = horses.get(e.getUniqueId().toString());
    				org.bukkit.entity.Horse bHorse = (org.bukkit.entity.Horse) e;
    				
    				if(!h.getOwner().equals(p.getName()) || !p.hasPermission("abeomisc.horse.modify")) {
    					p.sendMessage(ChatColor.RED + "Only a horse's owner or a staff member may change it's owner.");
    					return;
    				}
    				
    				bHorse.setOwner(op);
    				p.sendMessage(ChatColor.GREEN + "You've set the horse owner to " + ChatColor.BLUE + op.getName());
    				
    				hc.modifyOwner.remove(p.getName());
    				h.updateInfo(bHorse);
    				horses.put(h.getId().toString(), h);
    				saveData(h);
    				
    				return;
    			}
    			
    			if(hc.modifyRider.containsKey(p.getName())) {
    				ev.setCancelled(true);
    				
    				OfflinePlayer op = Bukkit.getOfflinePlayer(hc.modifyRider.get(p.getName()));
    				Horse h = horses.get(e.getUniqueId().toString());
    				
    				if(!h.getOwner().equals(p.getName())) {
    					p.sendMessage(ChatColor.RED + "Only the horse owner can change riders.");
    					return;
    				}
    					
    				if(h.isRider(op.getName())) {
    					h.removeRider(op.getName());
    					e.getPassenger().eject();
    					
    					p.sendMessage(ChatColor.GREEN + op.getName() + " has been removed as a rider.");
    				} else {
    					h.addRider(op.getName());
    				
    					p.sendMessage(ChatColor.GREEN + op.getName() + " has been added as a rider.");
    				}
    				
    				hc.modifyRider.remove(p.getName());
    				saveData(h);
    			}
    			
    		}
    	}
    }
    
    private String renderBar(String bar, double d, double e) {
        for(int i = 0; i < e; i++) {
            if(i <= d) {
                bar += ChatColor.GREEN + "||";
            } else {
                bar += ChatColor.RED + "||";
            }
        }
        return (bar + ChatColor.GRAY + "]");
    }

    public void saveData(Horse h) {
        FileConfiguration cfg = loadConfig("horses");

        cfg.set("horses." + h.getId() + ".owner", h.getOwner());
        cfg.set("horses." + h.getId() + ".riders", h.getRiders());
        cfg.set("horses." + h.getId() + ".tamed", Boolean.toString(h.isTamed()));
        cfg.set("horses." + h.getId() + ".variant", h.getVariant());
        cfg.set("horses." + h.getId() + ".style", h.getStyle());
        cfg.set("horses." + h.getId() + ".colour", h.getColour());
        cfg.set("horses." + h.getId() + ".jumpStrength", h.getJumpStrength());
        cfg.set("horses." + h.getId() + ".maxHealth", h.getMaxHealth());
        cfg.set("horses." + h.getId() + ".currentHealth", h.getCurrentHealth());
        cfg.set("horses." + h.getId() + ".age", h.getAge());
        cfg.set("horses." + h.getId() + ".saddled", Boolean.toString(h.isSaddled()));

        if(h.hasCustomName()) {
            cfg.set("horses." + h.getId() + ".customName", h.getCustomName());
        }

        if(h.hasArmour()) {
            cfg.set("horses." + h.getId() + ".armour", h.getArmour());
        }

        if(h.hasChest()) {
            cfg.set("horses." + h.getId() + ".chest", h.getChestContents());
        }
        
        saveConfig(cfg, "horses");
    }

    public void loadData() {
        FileConfiguration cfg = loadConfig("horses");
        horses.clear();
        
        if(cfg == null) {
            plugin.getLogger().info("Unable to find Horses.yml");
            return;
        }
        
        if(!cfg.contains("horses") || !cfg.isConfigurationSection("horses"))
            return; //Nothing to load
        
        Set<String> allHorses = cfg.getConfigurationSection("horses").getKeys(false);
        
        for(String horse : allHorses) {
            Horse h = new Horse(horse, cfg);
            horses.put(horse, h);
        }
        
        plugin.getLogger().info("Horses " + horses.size());
    }

    public void saveConfig(FileConfiguration cfg, String name) {
        try {
            cfg.save(new File(plugin.getDataFolder(), name + ".yml"));
        } catch(IOException e) {
            Logger.getLogger("Minecraft").severe("[AbeoMisc] Failed to save " + name + ".yml!");
            e.printStackTrace();
        }
    }

    public FileConfiguration loadConfig(String filename) {
        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), filename + ".yml"));
    }

}
