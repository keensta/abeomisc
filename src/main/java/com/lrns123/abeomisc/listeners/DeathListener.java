package com.lrns123.abeomisc.listeners;

import java.util.Random;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.lrns123.abeomisc.AbeoMisc;

public class DeathListener implements Listener{
	
	public AbeoMisc plugin;
	
	public DeathListener(AbeoMisc plugin) {
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent ev) {
		Player ply = (Player) ev.getEntity();
		Economy eco = plugin.getEconomy();
		
		if(eco == null)
		    return;
		
		double amount = eco.getBalance(ply.getName());
		
		if(amount >= 100) {
			Random r = new Random();
			double percent = 0.08 + (0.13 - 0.08) * r.nextDouble();
			double lAmount = (eco.getBalance(ply.getName())*percent);
			if(lAmount < 3000) {
				eco.withdrawPlayer(ply.getName(), lAmount);
				ply.sendMessage(ChatColor.GRAY + "You have lost " + ChatColor.RED + lAmount + ChatColor.GRAY + " Coins on death");
				if(ply.getKiller() instanceof Player) {
					Player klr = ply.getKiller();
					eco.depositPlayer(klr.getName(), lAmount/2);
					klr.sendMessage(ChatColor.GRAY + "You have found " + ChatColor.GREEN + lAmount + ChatColor.GRAY + " Coins on the body of " + ChatColor.RED + ply.getName());
				}
			} else {
				double percent2 = 0.85 + (0.85 - 0.99) * r.nextDouble();
				int lAmount2 = (int) (3000*percent2);
				eco.withdrawPlayer(ply.getName(), lAmount2);
				ply.sendMessage(ChatColor.GRAY + "You have lost " + ChatColor.RED + lAmount2 + ChatColor.GRAY + " Coins on death");
				if(ply.getKiller() instanceof Player) {
					Player klr = ply.getKiller();
					eco.depositPlayer(klr.getName(), lAmount2/2);
					klr.sendMessage(ChatColor.GRAY + "You have found " + ChatColor.GREEN + lAmount2/2 + ChatColor.GRAY + " Coins on the body of " + ChatColor.RED + ply.getName());
				}
			}
		}
	}

}
