package com.lrns123.abeomisc.gamestuff.util;

import java.util.ArrayList;
import java.util.List;

public class Frequency {
	
	private List<String> locations = new ArrayList<String>();
	
	public Frequency (List<String> locs) {
		this.setLocations(locs);
	}

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}
	
	public void addLocation(String s) {
	    if(this.locations.contains(s))
	        return;
		this.locations.add(s);
	}
	
	public void removeLocation(String s) {
		this.locations.remove(s);
	}
	
	public boolean hasLocation(String s) {
		return this.locations.contains(s);
	}

}
