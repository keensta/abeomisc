package com.lrns123.abeomisc.gamestuff.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class RedNetTimer implements Runnable {

	Location loc = null;
	int id;
	
	public RedNetTimer(Location loc) {
		this.loc = loc;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		
	 Block b = loc.getBlock();
	 
	 if(b.getType() == Material.REDSTONE_TORCH_ON || b.getType() == Material.REDSTONE_TORCH_OFF) {
		 byte data = b.getData();
		 b.setTypeIdAndData(Material.TORCH.getId(), data, true);
	 }	else if(b.getType() == Material.TORCH) {
			byte data = b.getData();
			b.setTypeIdAndData(Material.REDSTONE_TORCH_ON.getId(), data, true);
	 	}
		
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

}
