package com.lrns123.abeomisc.gamestuff.listeners;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.lrns123.abeomisc.AbeoMisc;


public class GameChestListener implements Listener{

	public AbeoMisc plugin;
	Logger log = Logger.getLogger("Minecraft");
	private BlockFace[] bf = {BlockFace.NORTH, BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH};

	public GameChestListener(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev) {
		
		if(!ev.hasBlock())
			return;
		
		Block b = ev.getClickedBlock();
		
		if(b.getType() == Material.CHEST) {
			for(BlockFace face : bf) {
				Block blockCheck = b.getRelative(face);
				if(blockCheck.getType() == Material.SIGN || blockCheck.getType() == Material.WALL_SIGN) {
					Sign sign = (Sign) blockCheck.getState();
					if(sign.getLine(0).equalsIgnoreCase("[GameChest]")) {
						String chestName = sign.getLine(1);
						Player p = ev.getPlayer();
						File f = new File(plugin.getDataFolder()+ File.separator +"gamechests", chestName+".yml");
						if(f.exists()) {
							//FileConfiguration chest = YamlConfiguration.loadConfiguration(f);
							//String chestInv = chest.getString("NBTInv");
							ev.setUseInteractedBlock(Result.DENY);
							ev.setCancelled(true);
						//	p.openInventory(InventoryUtil.fromBase64(chestInv));
						} else {
							p.sendMessage(ChatColor.RED + "GameChest inventory can't be found!");
							ev.setUseInteractedBlock(Result.DENY);
							ev.setCancelled(true);
						}
					}
					continue;
				}
			}
		}
		
	}
	
	@EventHandler
	public void signChange(SignChangeEvent ev) {
		String signTitle = ev.getLine(0);
		Player p = ev.getPlayer();
		if(signTitle == "[GameChest]" && !(p.hasPermission("abeomisc.game.gamechest"))) {
			ev.setCancelled(true);
			ev.setLine(0, "----");
			p.sendMessage(ChatColor.RED + "Insufficent permission!");
		}
	}
	
}
