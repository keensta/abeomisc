package com.lrns123.abeomisc.gamestuff.listeners;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Button;
import org.bukkit.material.Lever;

import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.gamestuff.util.Frequency;
import com.lrns123.abeomisc.gamestuff.util.RedNetTimer;

public class RedNetListener implements Listener {
	
	private AbeoMisc plugin;
	
	public HashMap<Integer, Frequency> frequencys = new HashMap<Integer, Frequency>();
	private static BlockFace lateralFaces[] = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
	
	private List<RedNetTimer> rednetTimers = new ArrayList<RedNetTimer>();
	public List<String> players = new ArrayList<String>();
	
	public RedNetListener(AbeoMisc plugin) {
		this.plugin = plugin;
		
		loadFrequencys();
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	public void saveFrequency(int i, Frequency f) {
	    FileConfiguration cfg = loadConfig("frequencys");
	    String compressedLocations = "";
	    
	    for(String s : f.getLocations()) {
	        compressedLocations = compressedLocations + s + ":";
	    }
	    
	    cfg.set("ids." + i + ".locations", compressedLocations);
	    saveConfig(cfg, "frequencys");
	}
	
	public void loadFrequencys() {
	    FileConfiguration cfg = loadConfig("frequencys");
	    frequencys.clear();
	    
	    if(cfg == null)
	        return;
	    
	    if(!cfg.contains("ids") || !cfg.isConfigurationSection("ids"))
	        return; //Nothing to load
	    
	    Set<String> ids = cfg.getConfigurationSection("ids").getKeys(false);
	    
	    for(String id : ids) {
	        String compressLocations = (String) cfg.get("ids." + id + ".locations");
	        String[] uncompressLocations = compressLocations.split(":");
	        List<String> locations = new ArrayList<String>();
	        
	        for(int j = 0; j < uncompressLocations.length; j++) {
	            locations.add(uncompressLocations[j]);
	        }
	        
	        Frequency f = new Frequency(locations);
	        frequencys.put(Integer.parseInt(id), f);
	    }
	}
	
	public void removeFrequency(int i, Frequency f) {
	    FileConfiguration cfg = loadConfig("frequencys");
	    frequencys.remove(f);
	    
	    cfg.set("ids." + i, null);
	    saveConfig(cfg, "frequencys");
	}
	
	public void saveConfig(FileConfiguration cfg, String name) {
        try {
            cfg.save(new File(plugin.getDataFolder(), name+".yml"));
        } catch (IOException e) {
            Logger.getLogger("Minecraft").severe("[AbeoMisc] Failed to save " + name +".yml!");
            e.printStackTrace();
        }
    }
	
	public FileConfiguration loadConfig(String filename) {
	    return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), filename+".yml"));
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onButtonPress(PlayerInteractEvent ev) {
		Block block = ev.getClickedBlock();
		Player p = ev.getPlayer();
		
		if(block == null)
			return;
		
    	if (block.getType() == Material.STONE_BUTTON || block.getType() == Material.WOOD_BUTTON || block.getType() == Material.LEVER) {
    		Logger log = Logger.getLogger("Minecraft");
    		
    		Block placedOn = (block.getType() == Material.LEVER ? block.getRelative(((Lever)block.getState().getData()).getAttachedFace())
    				: block.getRelative(((Button)block.getState().getData()).getAttachedFace()) );
    		
    		for (BlockFace face : lateralFaces) {
    			Block lateralBlock = placedOn.getRelative(face);
    			if (lateralBlock.getState() instanceof Sign) {
    				Sign sign = (Sign)lateralBlock.getState();
    				String tag = sign.getLine(0).toLowerCase();
    				
    				if(tag.equalsIgnoreCase("[RedNet]")) {
    					String line1 = sign.getLine(1);
    					String timer = sign.getLine(2);
    					
    					if(timer.length() == 0)
    						timer = "0";
    					
    					if(line1.contains(",")) {
    						String world = sign.getLine(3);
    						int time = Integer.parseInt(timer.trim());
    						String[] locS = line1.split(",");
    						
    						if(locS.length < 3) {
    							p.sendMessage(ChatColor.RED + "Imcomplete Location");
    							return;
    						}
    						
    						if(world.trim().length() == 0) {
    							p.sendMessage(ChatColor.RED + "World not set, Use line 4 to set the world the location is in." 
    									+ " Current world name: " + p.getWorld().getName());
    							return;
    						}
    						
							Location loc = new Location(Bukkit.getWorld(world), Integer.parseInt(locS[0]), 
									Integer.parseInt(locS[1]), Integer.parseInt(locS[2]));
							Block b = loc.getBlock();
							
							if(block.getType() == Material.LEVER) {
								log.info("Is lever");
								
	    						if(!(timer.equalsIgnoreCase("0"))) {
	    							sign.setLine(1, "0");
	        						timer = "0";
	        						p.sendMessage(ChatColor.RED + "Set sign timer to 0 because of using lever.");
	    						}
								
								Lever l = (Lever) block.getState().getData();
								
								if(l.isPowered()) {
									log.info("is powered");
									byte data = b.getData();
									b.setTypeIdAndData(Material.TORCH.getId(), data, true);
								} else {
									log.info("isnt powered");
									byte data = b.getData();
									b.setTypeIdAndData(Material.REDSTONE_TORCH_ON.getId(), data, true);
								}
								return;
							}
							
							if(b.getType() == Material.TORCH) {
								byte data = b.getData();
								b.setTypeIdAndData(Material.REDSTONE_TORCH_ON.getId(), data, true);
								if(time != 0) {
									RedNetTimer rdt = new RedNetTimer(loc);
									int id = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, rdt, (time * 20));
									rdt.setId(id);
								}
							} else if(b.getType() == Material.REDSTONE_TORCH_ON || b.getType() == Material.REDSTONE_TORCH_OFF) {
								byte data = b.getData();
								b.setTypeIdAndData(Material.TORCH.getId(), data, true);
							}
							return;
    					}
    					
    					if(frequencys.containsKey(Integer.parseInt(line1.trim()))) {
    						Frequency f = frequencys.get(Integer.parseInt(line1.trim()));
    						int time = Integer.parseInt(timer.trim());
    						
    						for(int i = 0; i < f.getLocations().size(); i++) {
    							String[] locS = f.getLocations().get(i).split(",");
    							
    							if(locS.length < 4) {
        							p.sendMessage(ChatColor.RED + "Imcomplete Location");
        							continue;
        						}
    							
    							if(Bukkit.getWorld(locS[3]) == null) {
    								return;
    							}
    							
    							Location loc = new Location(Bukkit.getWorld(locS[3]), Integer.parseInt(locS[0]), 
    									Integer.parseInt(locS[1]), Integer.parseInt(locS[2]));
    						
    							Block b = loc.getBlock();
    							
    							if(b.getType() == Material.TORCH) {
    								byte data = b.getData();
    								b.setTypeIdAndData(Material.REDSTONE_TORCH_ON.getId(), data, true);
    								if(time != 0) {
    									Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new RedNetTimer(loc), (time * 20));
    								}
    							} else if(b.getType() == Material.REDSTONE_TORCH_ON || b.getType() == Material.REDSTONE_TORCH_OFF) {
    								 byte data = b.getData();
    								 b.setTypeIdAndData(Material.TORCH.getId(), data, true);
    							}
    							continue;
    						}
    						
    					} else {
    						p.sendMessage(ChatColor.RED + "Location is not set right or Frequency doesn't exist");
    						return;
    					}
    					
    				}
    			}
    		}
    	}
	}
	
    @EventHandler
    public void onSignChange(SignChangeEvent ev) {
    	if (ev.getLine(0).equalsIgnoreCase("[RedNet]")) {
   			if (!ev.getPlayer().hasPermission("abeomisc.rednet")) {
				ev.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to create rednet signs");
				ev.getBlock().breakNaturally();
				ev.setCancelled(true);
			}
   		}
    }
    
    public void shutdown() {
    	for(RedNetTimer rdt : rednetTimers) {
    		rdt.run();
    		int id = rdt.getId();
    		if(id != -1 && plugin.getServer().getScheduler().isQueued(id) || plugin.getServer().getScheduler().isCurrentlyRunning(id)) {
    			plugin.getServer().getScheduler().cancelTask(id);
    		}
    	}
    }

    @EventHandler
    public void blockBreak(BlockBreakEvent ev) {
        Block b = ev.getBlock();
        Player p = ev.getPlayer();
        
        if(!players.contains(p.getName()))
            return;
        
        if(b.getType() == Material.TORCH || b.getType() == Material.REDSTONE_TORCH_ON) {
            if(p.getItemInHand().getType() == Material.STICK) {
                Location l = b.getLocation();
                int x = l.getBlockX();
                int y = l.getBlockY();
                int z = l.getBlockZ();
                String world = p.getWorld().getName();
                p.chat("/rednet addlocation " + x +","+ y +","+ z +","+ world);
                
                ev.setCancelled(true);
            }
        }
    }

}
