package com.lrns123.abeomisc.gamestuff;

import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.gamestuff.commands.ArrowCounterSystem;
import com.lrns123.abeomisc.gamestuff.commands.GameChestCommand;
import com.lrns123.abeomisc.gamestuff.commands.RedNetCommand;
import com.lrns123.abeomisc.gamestuff.commands.VirtualChest;
import com.lrns123.abeomisc.gamestuff.commands.WitherBoss;
import com.lrns123.abeomisc.gamestuff.listeners.GameChestListener;
import com.lrns123.abeomisc.gamestuff.listeners.RedNetListener;

public class GameManager {
	
	public AbeoMisc plugin;
	
    //Games
    public GameChestCommand GameChestCommand = null;
    public GameChestListener GameChestListener = null;
    public ArrowCounterSystem arrowSystem = null;
    public VirtualChest vc = null;
    public WitherBoss wb = null;
    
    //Wireless Redstone
    public RedNetCommand RedNetCommand = null;
    public RedNetListener RedNetListener = null;
	
	public GameManager(AbeoMisc plugin) {
		this.plugin = plugin;

        GameChestCommand = new GameChestCommand(plugin);
        GameChestListener = new GameChestListener(plugin);
        arrowSystem = new ArrowCounterSystem(plugin);
        vc = new VirtualChest(plugin);
        wb = new WitherBoss(plugin);
        
        RedNetCommand = new RedNetCommand(plugin);
        RedNetListener = new RedNetListener(plugin);
	}
	
	
	public void shutdown() {
		arrowSystem.shutdown();
		RedNetListener.shutdown();
	}
	
	public RedNetListener getRedNetListener() {
		return RedNetListener;
	}

}
