package com.lrns123.abeomisc.gamestuff.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.lrns123.abeomisc.AbeoMisc;

public class GameChestCommand implements CommandExecutor {
	
	//Adding chest saving support Later & Allow loading into inventory 
	
	public AbeoMisc plugin;
	
	public GameChestCommand(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getCommand("gamechest").setExecutor(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player))
			return true;
		
		Player p = (Player) sender;
		
		if(p.hasPermission("abeomisc.game.gamechest")) {
			
			if(args.length < 1) {
				p.sendMessage(ChatColor.RED + "Usage: '/gamechest [Name]' or '/gamechest [name] -c' to clone a chest");
				return true;
			} else {
			
				if(args.length == 1) {
					String gameChestName = args[0];
					File f = new File(plugin.getDataFolder()+ File.separator +"gamechests", gameChestName+".yml");
					if(f.exists()) {
						p.sendMessage(ChatColor.RED + gameChestName + " already exists!");
						return true;
					} else {
						FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
						//cfg.set("NBTInv", InventoryUtil.toBase64(InventoryUtil.getContentInventory(p.getInventory())));
						try {
							cfg.save(f);
						} catch (IOException e) {
							e.printStackTrace();
						}
						p.sendMessage(ChatColor.GREEN + gameChestName + " has been created");
					}
				}
				
				String gameChestName = args[0];
				File f = new File(plugin.getDataFolder()+ File.separator +"gamechests", gameChestName+".yml");
				if(args[1].equalsIgnoreCase("-c")) {
					Block b = p.getTargetBlock(null, 100);
					
					if(b.getType() == Material.CHEST) {
					//	Chest c = (Chest) b.getState();
						if(f.exists()) {
							p.sendMessage(ChatColor.RED + gameChestName + " already exists!");
							return true;
						} else {
							FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
							//cfg.set("NBTInv", InventoryUtil.toBase64(c.getInventory()));
							try {
								cfg.save(f);
							} catch (IOException e) {
								e.printStackTrace();
							}
							p.sendMessage(ChatColor.GREEN + gameChestName + " has been created");
							return true;
						}
					} else {
						p.sendMessage(ChatColor.RED + "You have to look at a chest for this to work!");
						return true;
					}
				}
				
			}
			
		} else {
			p.sendMessage(ChatColor.RED + "Insufficent permission!");
		}
		
		return false;
	}
	
	
	

}

