package com.lrns123.abeomisc.gamestuff.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;

import com.lrns123.abeomisc.AbeoMisc;

public class WitherBoss implements CommandExecutor{
    
    @SuppressWarnings("unused")
    private AbeoMisc plugin;
    List<Wither> currentBosses = new ArrayList<Wither>();
    
    public WitherBoss(AbeoMisc plugin) {
        this.plugin = plugin;
        
        plugin.getCommand("witherboss").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        if(!(sender instanceof Player))
            return true;
        
        Player p = (Player) sender;
        
        if(!p.hasPermission("abeomisc.witherboss")) {
            p.sendMessage(ChatColor.RED + "You are unable to create WitherBosses!");
            return true;
        }
        
        Wither w = (Wither) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.WITHER);
        
        w.setCustomName(ChatColor.RED + "WitherBoss");
        w.setCustomNameVisible(true);
        
        if(args.length == 0) {
            w.setMaxHealth(w.getMaxHealth());
            w.setHealth(w.getMaxHealth());
            
            currentBosses.add(w);
        } else {
            int i = 0;
            
            try {
                i = Integer.parseInt(args[0]);
            } catch(NumberFormatException e) {
                p.sendMessage(ChatColor.RED + "Usage: '/witherboss [level]'");
            }
            
            w.setCustomName(ChatColor.RED + "WItherBoss");
            
            w.setMaxHealth(w.getMaxHealth() * i);
            w.setHealth(w.getMaxHealth());
            
            currentBosses.add(w);
        }
        
      
        
        return false;
    }

}
