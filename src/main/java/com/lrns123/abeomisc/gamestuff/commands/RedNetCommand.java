package com.lrns123.abeomisc.gamestuff.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.gamestuff.util.Frequency;

public class RedNetCommand implements CommandExecutor, Listener {
	
	public AbeoMisc plugin;
	
	public HashMap<Integer, List<String>> fLocation = new HashMap<Integer, List<String>>();
	public HashMap<String, Integer> fNumber = new HashMap<String, Integer>();

	public RedNetCommand(AbeoMisc plugin) {
		this.plugin = plugin;
		
		plugin.getCommand("rednet").setExecutor(this);
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if(!(sender instanceof Player))
			return true;

		Player p = (Player) sender;

		if(args.length == 0) {
			p.sendMessage(ChatColor.RED + "Usage '/rednet'"  + " 'Not completed yet' ");
			return true;
		}
		
		if(args[0].equalsIgnoreCase("create")) {
			if(args.length < 2) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet create [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			String frequency = args[1];
			int fNum;
			
			try {
				fNum = Integer.parseInt(frequency);
			} catch (NumberFormatException e) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet create [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			if(plugin.getGameManager().getRedNetListener().frequencys.containsKey(fNum)) {
				p.sendMessage(ChatColor.RED + "" + fNum + " is already taken as a frequency!");
				return false;
			}
			
			fNumber.put(p.getName(), fNum);
			p.sendMessage(ChatColor.GREEN + "" + fNum + " now exists.");
			p.sendMessage(ChatColor.GREEN + "Do '/rednet addloction [Location]' Location must be like '-45,29,199,worldname'");
			return true;
		} 
		
		if(args[0].equalsIgnoreCase("delete")) {
			if(args.length < 2) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet delete [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			String frequency = args[1];
			int fNum;
			
			try {
				fNum = Integer.parseInt(frequency);
			} catch (NumberFormatException e) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet create [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			if(!plugin.getGameManager().getRedNetListener().frequencys.containsKey(fNum)) {
				p.sendMessage(ChatColor.RED + "" + fNum + " doesn't exist!");
				return false;
			}
			
			if(fNumber.containsKey(p.getName()))
				fNumber.remove(p.getName());
			
			Frequency f = plugin.getGameManager().getRedNetListener().frequencys.get(fNum);
			
			p.sendMessage(ChatColor.GREEN + "" + fNum + " has been deleted.");
			plugin.getGameManager().getRedNetListener().frequencys.remove(fNum);
			plugin.getGameManager().getRedNetListener().removeFrequency(fNum, f);
			return true;
		}
		
		if(args[0].equalsIgnoreCase("select")) {
			
			if(args.length < 2) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet select [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			int frequency;
			
			try {
				frequency = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet select [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			if(!plugin.getGameManager().getRedNetListener().frequencys.containsKey(frequency)) {
				p.sendMessage(ChatColor.RED + "Frequency doesn't exist");
				return false;
			}
			
			fNumber.put(p.getName(), frequency);
			p.sendMessage(ChatColor.GREEN + "" + frequency + " has now been selected.");
			p.sendMessage(ChatColor.GREEN + "Do '/rednet addloction [Location]' Location must be like '-45,29,199,worldname'");
		}
		
		if(args[0].equalsIgnoreCase("addlocation")) {
			
			if(!fNumber.containsKey(p.getName())) {
				p.sendMessage(ChatColor.RED + "Use '/rednet create [FrequencyNumber]' first. or '/rednet select [FrequencyNumber]'");
				return false;
			}
			
			int frequency = fNumber.get(p.getName());
			
			if(args.length < 2) {
				p.sendMessage(ChatColor.RED + "Usage: '/rednet addloction [Location]' Location must be like '-45,29,199,worldname'");
				return false;
			}
			
			String location = args[1];
			String[] splitLoc = args[1].split(",");
			
			if(splitLoc.length < 4) {
				p.sendMessage(ChatColor.RED + "Location argument must be 4 arguments. 'x,y,z,worldname'");
				return false;
			}
			
			List<String> locs = (fLocation.containsKey(frequency) ? fLocation.get(frequency) : new ArrayList<String>());
			
			locs.add(location);
			
			fLocation.put(frequency, locs);
			p.sendMessage(ChatColor.GREEN + "" + location + " has been added to " + frequency + ".");
			p.sendMessage(ChatColor.GREEN + "If you're done do '/rednet done'");
		}
		
		if(args[0].equalsIgnoreCase("removelocation")) {
			
			if(!fNumber.containsKey(p.getName())) {
				p.sendMessage(ChatColor.RED + "Use '/rednet create [FrequencyNumber]' first. or '/rednet select [FrequencyNumber]'");
				return false;
			}
			
			int frequency = fNumber.get(p.getName());
			
			if(args.length < 2) {
				p.sendMessage(ChatColor.RED + "Usage: '/rednet removeloction [Location]' Location must be like '-45,29,199,worldname'");
				return false;
			}
			
			String location = args[1];
			String[] splitLoc = args[1].split(",");
			
			if(splitLoc.length < 4) {
				p.sendMessage(ChatColor.RED + "Location argument must be 4 arguments. 'x,y,z,worldname'");
				return false;
			}
			
			List<String> locs = (fLocation.containsKey(frequency) ? fLocation.get(frequency) : new ArrayList<String>());
			
			locs.remove(location);
			
			fLocation.put(frequency, locs);
			p.sendMessage(ChatColor.GREEN + "" + location + " has been removed from " + frequency + ".");
			p.sendMessage(ChatColor.GREEN + "If you're done do '/rednet done'");
		}
		
		if(args[0].equalsIgnoreCase("done")) {
			
			if(!fNumber.containsKey(p.getName())) {
				p.sendMessage(ChatColor.RED + "Use '/rednet create [FrequencyNumber]' first.");
				return false;
			}
			
			int frequency = fNumber.get(p.getName());
			
			Frequency f = new Frequency(fLocation.get(frequency));
			
			if(plugin.getGameManager().getRedNetListener().frequencys.containsKey(frequency)) {
				f = plugin.getGameManager().getRedNetListener().frequencys.get(frequency);
			}
			
			if(plugin.getGameManager().getRedNetListener().players.contains(p.getName()))
			    plugin.getGameManager().getRedNetListener().players.remove(p.getName());
			
			for(String s : fLocation.get(frequency)) {
			    f.addLocation(s);
			}
			
			plugin.getGameManager().getRedNetListener().saveFrequency(frequency, f);
			plugin.getGameManager().getRedNetListener().frequencys.put(frequency, f);
			
			if(fNumber.containsKey(p.getName()))
				fNumber.remove(p.getName());
			
			p.sendMessage(ChatColor.GREEN + "Frequency updated!");
		}
		
		if(args[0].equalsIgnoreCase("listFrequencys")) {
			
			HashMap<Integer, Frequency> frequencys = plugin.getGameManager().getRedNetListener().frequencys;
			
			if(frequencys.size() == 0) {
				p.sendMessage(ChatColor.RED + " No Frequencys exist yet!");
				return false;
			}
			
			StringBuilder sb = new StringBuilder();
			for(Integer f : frequencys.keySet()) {
				sb.append(f);
				sb.append(", ");
			}
			p.sendMessage(ChatColor.BLUE + "Current Frequencys:");
			p.sendMessage(sb.toString());
			return true;
		}
		
		if(args[0].equalsIgnoreCase("listLocations")) {
			if(args.length < 2) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet listLocations [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			String frequency = args[1];
			int fNum;
			
			try {
				fNum = Integer.parseInt(frequency);
			} catch (NumberFormatException e) {
				p.sendMessage(ChatColor.RED + "Usage '/rednet create [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
				return false;
			}
			
			if(!plugin.getGameManager().getRedNetListener().frequencys.containsKey(fNum)) {
				p.sendMessage(ChatColor.RED + "" + fNum + " doesn't exist!");
				return false;
			}
			
			Frequency f = plugin.getGameManager().getRedNetListener().frequencys.get(fNum);
			
			StringBuilder sb = new StringBuilder();
			for(String s  : f.getLocations()) {
				sb.append(s);
				sb.append(ChatColor.BLUE + ", " + ChatColor.WHITE);
				
			}
			
			p.sendMessage(ChatColor.BLUE + "Current Frequency Locations:");
			p.sendMessage(sb.toString());
			return true;
		}
		
		if(args[0].equalsIgnoreCase("activateSelection")) {
		    if(args.length < 2) {
		        p.sendMessage(ChatColor.RED + "Usage '/rednet activateSelection [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
		        return false;
		    }
		    
		    String frequency = args[1];
		    int fNum;
		    
	        try {
	            fNum = Integer.parseInt(frequency);
	        } catch (NumberFormatException e) {
	            p.sendMessage(ChatColor.RED + "Usage '/rednet activateSelection [FrequencyNumber]' #FrequencyNumber must be a NUMBER");
	            return false;
	        }
	        
	        if(!plugin.getGameManager().getRedNetListener().frequencys.containsKey(fNum)) {
                p.sendMessage(ChatColor.RED + "Frequency doesn't exist");
                return false;
            }
            
            fNumber.put(p.getName(), fNum);
            
	        plugin.getGameManager().getRedNetListener().players.add(p.getName());
	        p.sendMessage(ChatColor.GREEN + "Location selection active. Use '/rednet done' once completed");
		}
		
		return true;
	}

}
