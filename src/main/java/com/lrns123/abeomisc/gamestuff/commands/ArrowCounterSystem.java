package com.lrns123.abeomisc.gamestuff.commands;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.lrns123.abeomisc.AbeoMisc;

public class ArrowCounterSystem implements CommandExecutor, Listener {

	Logger log = Logger.getLogger("Minecraft");
	
	public AbeoMisc plugin;
	List<String> activePlayers = new LinkedList<String>();
	HashMap<String, Scoreboard> playerScoreboard = new HashMap<String, Scoreboard>();
	ScoreboardManager manager = Bukkit.getScoreboardManager();

	public ArrowCounterSystem(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getCommand("arrowcount").setExecutor(this);
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	
	/**
	 * Shuts down all players ArrowCounters.
	 */
	public void shutdown() {
		for(String pS : activePlayers) {
			Player p = Bukkit.getPlayer(pS);
			if(p == null || !p.isOnline())
				continue;
			else {
				disableScoreboard(p);
			}
		}
		activePlayers.clear();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if(!(sender instanceof Player))
			return true;

		Player p = (Player) sender;

		if(args.length == 0) {
			p.sendMessage(ChatColor.RED + "Usage '/arrowcount [enable/disable]");
			return true;
		}

		if(args[0].equalsIgnoreCase("enable")) {
			if(activePlayers.contains(p.getName())) {
				p.sendMessage(ChatColor.RED + "ArrowCount is already active");
				return true;
			}
			p.sendMessage(ChatColor.GREEN + "ArrowCount enabled");
			updateScoreboard(p, getArrowCount(p));
			activePlayers.add(p.getName());
			return true;
		}

		if(args[0].equalsIgnoreCase("disable")) {
			if(activePlayers.contains(p.getName())) {
				disableScoreboard(p);
				p.sendMessage(ChatColor.GREEN + "ArrowCount disabled");
				activePlayers.remove(p.getName());
				return true;
			} else {
				p.sendMessage(ChatColor.RED + "You do not have ArrowCount enabled.");
				return true;
			}
		}

		return true;
	}

	@EventHandler
	public void shootArrow(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(p.getItemInHand().getType() == Material.BOW) {
			if(!activePlayers.contains(p.getName()))
				return;
			updateScoreboard(p, getArrowCount(p));
		}
	}
	
	@EventHandler
	public void shootA(ProjectileLaunchEvent ev) {
		Projectile proj = ev.getEntity();
		LivingEntity le = proj.getShooter();
		EntityType et = proj.getType();
		Player p = null;
		
		if(le instanceof Player)
			p = (Player) le;
		
		if(p == null)
			return; 
		
		if(et.equals(EntityType.ARROW) && activePlayers.contains(p.getName())) {
			updateScoreboard(p, (getArrowCount(p) - 1));
		}
			
	}

	@EventHandler
	public void arrowPickup(PlayerPickupItemEvent ev) {
		Player p = ev.getPlayer();
		ItemStack i = new ItemStack(Material.ARROW);
		ItemStack pI = new ItemStack(ev.getItem().getItemStack());
		
		i.setAmount(pI.getAmount());
		
		if(activePlayers.contains(p.getName()) && pI.equals(i)) {
			ev.setCancelled(true);
			ev.getItem().remove();
			p.getInventory().addItem(pI);
			updateScoreboard(p, getArrowCount(p));
		}
		
	}

	@EventHandler
	public void arrowDrop(PlayerDropItemEvent ev) {
		Player p = ev.getPlayer();
		ItemStack i = new ItemStack(Material.ARROW);
		ItemStack dI = new ItemStack(ev.getItemDrop().getItemStack());
		
		i.setAmount(dI.getAmount());
		
		if(activePlayers.contains(p.getName()) && dI.equals(i)) {
			updateScoreboard(p, getArrowCount(p));
		}
		
	}

	/**
	 * Updates the Scoreboard with new data.
	 * @param p Player you wish to update.
	 * @param amount The current amount of arrows.
	 */
	private void updateScoreboard(Player p, int amount) {
		if(!hasScoreboard(p))
			createScoreboard(p);
		
		Scoreboard board = getScoreboard(p);
		Objective obj = board.getObjective("Arrow");
		Score score = obj.getScore(Bukkit.getOfflinePlayer(""));
		
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.setDisplayName("Arrows");
		score.setScore(amount);
		p.setScoreboard(board);
		
	}

	/**
	 * Removes the scoreboard from players view.
	 * @param p Player you want to disable the scoreboard for.
	 */
	private void disableScoreboard(Player p) {
		p.setScoreboard(manager.getNewScoreboard());
		activePlayers.remove(p.getName());
	}

	/**
	 * Gets the amount of arrows in a players inventory.
	 * @param p Player you want to search.
	 * @return Returns amount of arrows player has. (Int)
	 */
	private int getArrowCount(Player p) {
		Inventory inv = p.getInventory();
		int arrows = 0;
		
		for(int i = 0; i < inv.getSize(); i++) {
			ItemStack is = inv.getItem(i);
			if(is == null)
				continue;
			if(is.getType() == Material.ARROW) {
				arrows += is.getAmount();
			}
		}
		
		return arrows;
	}
	
	/**
	 * Creates a scoreboard.
	 * @param p Player you wish to create scoreboad for.
	 */
	private void createScoreboard(Player p) {
		Scoreboard board = manager.getNewScoreboard();
		
		board.registerNewObjective("Arrow", "Quiver");
		playerScoreboard.put(p.getName(), board);
	}
	
	/**
	 * Checks if player has a scoreboard.
	 * @param p Player you want to check.
	 * @return True is they have one, Otherwise false.
	 */
	private boolean hasScoreboard(Player p) {
		return playerScoreboard.containsKey(p.getName());
	}
	
	/**
	 * Gets the players scoreboard.
	 * @param p Players scoreboard you want.
	 * @return Scoreboard.
	 */
	public Scoreboard getScoreboard(Player p) {
		return playerScoreboard.get(p.getName());
	}

}
