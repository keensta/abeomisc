package com.lrns123.abeomisc.gamestuff.commands;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.gamestuff.util.InventoryUtil;

public class VirtualChest implements CommandExecutor, Listener {

    AbeoMisc plugin;
    private static BlockFace[] AllFaces = { BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST, BlockFace.UP };
    HashMap<String, Inventory> openInvs = new HashMap<String, Inventory>();
    HashMap<String, String> areaInv = new HashMap<String, String>();

    public VirtualChest(AbeoMisc plugin) {
        this.plugin = plugin;

        plugin.getCommand("virtualchest").setExecutor(this);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player))
            return true;

        Player p = (Player) sender;

        if(!p.hasPermission("abeomisc.virtualchest")) {
            p.sendMessage(ChatColor.RED + "Insufficent permissions");
            return true;
        }

        if(args.length == 0) {
            return true;
        }
        
        if(args[0].equalsIgnoreCase("clear")) {
            if(args.length < 3) {
                p.sendMessage(ChatColor.RED + "Use /vc clear [Playername] [AreaName]");
                return true;
            }
            
            String area = args[2];
            String player = args[1];
            FileConfiguration cfg = loadConfig(player, area);
            
            if(!(cfg.contains("inventory"))) {
                p.sendMessage(ChatColor.RED + "They haven't used this yet");
                return true;
            }
            
            cfg.set("inventory", null);
            saveConfig(cfg, player, area);
        }
        
        if(args[0].equalsIgnoreCase("see")) {
            if(args.length < 3) {
                p.sendMessage(ChatColor.RED + "Use /vc see [Playername] [AreaName]");
                return true;
            }
            
            String area = args[2];
            String player = args[1];
            FileConfiguration cfg = loadConfig(player, area);
            
            if(!(cfg.contains("inventory"))) {
                p.sendMessage(ChatColor.RED + "They haven't used this yet");
                return true;
            }
            
            try {
                p.openInventory(InventoryUtil.fromBase64(cfg.getString("inventory")));
            } catch(IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent ev) {
        Block cb = ev.getClickedBlock();
        Player p = ev.getPlayer();

        if(cb == null)
            return;

        if(cb.getType() == Material.CHEST) {
            Block b = getSign(cb);
            
            if(b == null)
                return;
         
            Sign sign = (Sign) b.getState();
            
            if(sign == null)
                return;
            
            String line0 = sign.getLine(0);
            String line1 = sign.getLine(1);
            
            if(line0.equalsIgnoreCase("[VirtualChest]")) {
                FileConfiguration cfg = loadConfig(p.getName(), line1);

                Inventory newInv = null;
                
                if(!(cfg.contains("inventory")))
                    newInv = Bukkit.createInventory(p, 36);
                else {
                    try {
                        newInv = InventoryUtil.fromBase64(cfg.getString("inventory"));
                    } catch(IOException e) {
                        e.printStackTrace();
                    }
                    
                }
                ev.setUseInteractedBlock(Result.DENY);
                ev.setCancelled(true);
                
                p.openInventory(newInv);
                openInvs.put(p.getName(), newInv);
                areaInv.put(p.getName(), line1);
            }
        }
    }
    
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent ev) {
        Player p = (Player) ev.getPlayer();
        Inventory inv = ev.getInventory();
        
        if(openInvs.containsKey(p.getName())) {
            FileConfiguration cfg = loadConfig(p.getName(), areaInv.get(p.getName()));
            
            cfg.set("inventory", InventoryUtil.toBase64(inv));
            saveConfig(cfg, p.getName(), areaInv.get(p.getName()));
            
            openInvs.remove(p.getName());
            areaInv.remove(p.getName());
        }
    }

    private Block getSign(Block block) {
        for(BlockFace faces : AllFaces) {
            Block checkB = block.getRelative(faces);
            if(checkB.getType() == Material.WALL_SIGN || checkB.getType() == Material.SIGN
                    || checkB.getType() == Material.SIGN_POST) {
                return checkB;
            }
        }
        return null;
    }
    
    public void saveConfig(FileConfiguration cfg, String name, String folder) {
        try {
            cfg.save(new File(plugin.getDataFolder() +File.separator+ folder, name+".yml"));
        } catch (IOException e) {
            Logger.getLogger("Minecraft").severe("[AbeoMisc] Failed to save " + name +".yml!");
            e.printStackTrace();
        }
    }
    
    public FileConfiguration loadConfig(String filename, String folder) {
        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() +File.separator+ folder, filename+".yml"));
    }

}
