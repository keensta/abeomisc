package com.lrns123.abeomisc;

import java.util.logging.Logger;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.lrns123.abeomisc.commands.CopyBookCommand;
import com.lrns123.abeomisc.commands.EventCommand;
import com.lrns123.abeomisc.commands.HorseCommand;
import com.lrns123.abeomisc.commands.NamePetCommand;
import com.lrns123.abeomisc.commands.TellCommand;
import com.lrns123.abeomisc.commands.TestCommand;
import com.lrns123.abeomisc.commands.WarpCommand;
import com.lrns123.abeomisc.gamestuff.GameManager;
import com.lrns123.abeomisc.listeners.BedrockListener;
import com.lrns123.abeomisc.listeners.BlockDropListener;
import com.lrns123.abeomisc.listeners.DeathListener;
import com.lrns123.abeomisc.listeners.DognameListener;
import com.lrns123.abeomisc.listeners.DragonEgg;
import com.lrns123.abeomisc.listeners.HorseListener;
import com.lrns123.abeomisc.listeners.PlayerHead;
import com.lrns123.abeomisc.listeners.PotionListener;
import com.lrns123.abeomisc.listeners.VoidDamage;
import com.lrns123.abeomisc.listeners.WatchListener;
import com.lrns123.abeomisc.listeners.WelcomeMessage;
import com.lrns123.abeomisc.recipes.RecipesAddOn;

public class AbeoMisc extends JavaPlugin {

	Logger log = Logger.getLogger("Minecraft");

	public WelcomeMessage modWelcomeMsg = null;
	public WarpCommand modHomeCmd = null;
	public RecipesAddOn modRecipes = null;
	public TellCommand modTellCommand = null;
	public NamePetCommand modNamePetCommand = null;
	public EventCommand modEventCommand = null;
	public WatchListener modWatch = null;
	public DragonEgg modEgg = null;
	public PlayerHead modPlyHead = null;
	public PotionListener modPotListener = null;
	public DeathListener modDListener = null;
	public VoidDamage modVoidDamage = null;
	public BedrockListener modBedrockListener = null;
	public BlockDropListener modBlockDropLis = null;
	public CopyBookCommand modCopyBook = null;
	public DognameListener modDognameListener = null;
	public TestCommand modTest = null;
	public HorseCommand modHorse = null;
	public HorseListener modHorseL = null;

	// Game Manager
	public GameManager gameManager = null;

	private Chat chat;
	private Economy eco;

	@Override
	public void onDisable() {
		gameManager.shutdown();
		log.info(this.getDescription().getFullName() + " disabled");
	}

	@Override
	public void onEnable() {

		setupChat();
		setupEconomy();

		registarClasses();

		gameManager = new GameManager(this);

		log.info(this.getDescription().getFullName() + " enabled");
	}


	private boolean setupChat() {

		RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if(chatProvider != null) {
			chat = chatProvider.getProvider();
		}

		return (chat != null);
	}

	public Chat getChat() {
		return chat;
	}

	private boolean setupEconomy() {
		final RegisteredServiceProvider<Economy> economyProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if(economyProvider != null) {
			eco = economyProvider.getProvider();
		}

		return (eco != null);
	}

	public Economy getEconomy() {
		return eco;
	}

	public NamePetCommand getNamePetCommand() {
		return modNamePetCommand;
	}
	
	public HorseCommand getHorseCommand() {
	    return modHorse;
	}
	
	public BedrockListener getBedRockListener() {
		return modBedrockListener;
	}

	public GameManager getGameManager() {
		return gameManager;
	}
	
    //Moved here to keep all this clutter at the bottom..
	private void registarClasses() {
		modBedrockListener = new BedrockListener(this);
		modTellCommand = new TellCommand(this);
		modEventCommand = new EventCommand(this);
		modNamePetCommand = new NamePetCommand(this);
		modWelcomeMsg = new WelcomeMessage(this);
		modHomeCmd = new WarpCommand(this);
		modRecipes = new RecipesAddOn();
		modWatch = new WatchListener(this);
		modEgg = new DragonEgg(this);
		modPlyHead = new PlayerHead(this);
		modPotListener = new PotionListener(this);
		modDListener = new DeathListener(this);
		modVoidDamage = new VoidDamage(this);
		modBlockDropLis = new BlockDropListener(this);
		modCopyBook = new CopyBookCommand(this);
		modDognameListener = new DognameListener(this);
		modTest = new TestCommand(this);
		modHorse = new HorseCommand(this);
		modHorseL = new HorseListener(this);
	}

}