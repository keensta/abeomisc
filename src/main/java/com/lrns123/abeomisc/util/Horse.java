package com.lrns123.abeomisc.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

public class Horse {
    
     private UUID id = null;
     private String owner;
     private List<String> riders = new ArrayList<String>();
     private boolean tamed = false;
     private String variant = "HORSE";
     private String style = "NONE";
     private String colour = "GRAY";
     private Double jumpStrength = 0.0;
     private Double maxHealth = 0.0;
     private Double currentHealth = 0.0;
     private int age = 1;
     private boolean saddled = false;
     
     //Changble Vars
     private String customName = "";
     private ItemStack armour = null;
     private ItemStack[] chestContents = null;
     
     public Horse(org.bukkit.entity.Horse horse, String owner) {
         id = horse.getUniqueId();
         this.owner = owner;
         tamed = true;
         variant = horse.getVariant().toString();
         style = horse.getStyle().toString();
         colour = horse.getColor().toString();
         jumpStrength = horse.getJumpStrength();
         maxHealth = horse.getMaxHealth();
         currentHealth = horse.getHealth();
         age = horse.getAge();
         
         if(horse.getInventory().getSaddle() == null) {
             saddled = false;
         } else {
             saddled = true;
         }
         
         customName = horse.getCustomName();
         armour = horse.getInventory().getArmor();
         chestContents = horse.getInventory().getContents();
     }
     
     public Horse(String id, FileConfiguration cfg) {
         this.setId(UUID.fromString(id));
     }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public boolean isTamed() {
        return tamed;
    }

    public void setTamed(boolean tamed) {
        this.tamed = tamed;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Double getJumpStrength() {
        return jumpStrength;
    }

    public void setJumpStrength(Double jumpStrength) {
        this.jumpStrength = jumpStrength;
    }

    public Double getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(Double maxHealth) {
        this.maxHealth = maxHealth;
    }

    public Double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(Double currentHealth) {
        this.currentHealth = currentHealth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public boolean hasCustomName() {
        if(customName == null)
            return false;
        if(customName.length() == 0)
            return false;
        return true;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public boolean isSaddled() {
        return saddled;
    }

    public void setSaddled(boolean saddled) {
        this.saddled = saddled;
    }

    public boolean hasArmour() {
        if(armour == null)
            return false;
        return true;
    }
    
    public ItemStack getArmour() {
        return armour;
    }

    public void setArmour(ItemStack armour) {
        this.armour = armour;
    }
    
    public boolean hasChest() {
        if(chestContents == null)
            return false;
        return true;
    }

    public ItemStack[] getChestContents() {
        return chestContents;
    }

    public void setChestContents(ItemStack[] chestContents) {
        this.chestContents = chestContents;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<String> getRiders() {
        return riders;
    }

    public void setRiders(List<String> riders) {
        this.riders = riders;
    }
    
    public boolean isRider(String user) {
    	return riders.contains(user);
    }
    
    public void addRider(String user) {
    	riders.add(user);
    }
    
    public void removeRider(String user) {
    	riders.remove(user);
    }

    public void updateInfo(org.bukkit.entity.Horse horse) {
        id = horse.getUniqueId();
        this.owner = horse.getOwner().getName();
        tamed = true;
        variant = horse.getVariant().toString();
        style = horse.getStyle().toString();
        colour = horse.getColor().toString();
        jumpStrength = horse.getJumpStrength();
        maxHealth = horse.getMaxHealth();
        currentHealth = horse.getHealth();
        age = horse.getAge();
        
        if(horse.getInventory().getSaddle() == null) {
            saddled = false;
        } else {
            saddled = true;
        }
        
        customName = horse.getCustomName();
        armour = horse.getInventory().getArmor();
        chestContents = horse.getInventory().getContents();
    }

}
