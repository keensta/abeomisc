package com.lrns123.abeomisc.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.runnables.WarpRunnable;

public class EventCommand implements CommandExecutor {

	private AbeoMisc plugin;

	public EventCommand(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getCommand("event").setExecutor(this);
	}

	private Vector getVector(FileConfiguration cfg, String path) {
		try {
			List<Integer> list = cfg.getIntegerList(path);
			if(list == null || list.size() != 3)
				return new Vector();

			return new Vector(list.get(0), list.get(1), list.get(2));
		} catch (Exception e) {
			return new Vector();
		}
	}

	private void setVector(FileConfiguration cfg, String path, Vector vec) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(vec.getBlockX());
		list.add(vec.getBlockY());
		list.add(vec.getBlockZ());

		cfg.set(path, list.toArray(new Integer[0]));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player))
			return true;
		Player ply = (Player) sender;

		if(args.length == 0) {
			ply.sendMessage(ChatColor.RED + "Something was entered wrong.");
			if(ply.isOp()) {
				ply.sendMessage(ChatColor.BLUE + "Try these commands");
				ply.sendMessage(ChatColor.GREEN + "/event set [EventName]" + ChatColor.BLUE + " - This is used to set new Events and there teleport locations");
				ply.sendMessage(ChatColor.GREEN + "/event [Enable/Disable [EventName]" + ChatColor.BLUE + " - This is used to enable/disable events and there teleports");
				ply.sendMessage(ChatColor.GREEN + "/event [EventName]" + ChatColor.BLUE + " - This will teleport you to the event location");
				ply.sendMessage(ChatColor.GREEN + "/event list" + ChatColor.BLUE + " - This will list all current events available");
				return true;
			} else {
				ply.sendMessage(ChatColor.BLUE + "Try these commands");
				ply.sendMessage(ChatColor.GREEN + "/event [EventName]" + ChatColor.BLUE + " This will teleport you to the event location");
				ply.sendMessage(ChatColor.GREEN + "/event list" + ChatColor.BLUE + " - This will list all current events available");
				return true;
			}
		}

		if(args.length >= 1) {
			if(args[0].equalsIgnoreCase("list")) {
				FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "event.yml"));
				if(cfg.contains("Eventlist")) {
					Set<String> Events = cfg.getConfigurationSection("Eventlist").getKeys(false);

					StringBuilder sb = new StringBuilder();
					for(String events : Events) {
						sb.append(events);
						sb.append(", ");
					}
					ply.sendMessage(ChatColor.BLUE + "Available events list:");
					ply.sendMessage(ChatColor.BLUE + sb.toString());
				} else {
					ply.sendMessage(ChatColor.RED + "No current events are available");
				}
			} else if(args[0].equalsIgnoreCase("set")) {

				if(args.length < 2) {
					ply.sendMessage(ChatColor.RED + "Not enought arguments. Do" + ChatColor.GREEN + " /event set [EventName]");
				} else {
					if(ply.hasPermission("abeomisc.event")) {
						String EventName = args[1];
						FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "event.yml"));
						setVector(cfg, EventName + ".pos", ((Player) sender).getLocation().toVector());
						cfg.set(EventName + ".world", ((Player) sender).getLocation().getWorld().getName());
						cfg.set(EventName + ".state", true);
						cfg.set("Eventlist." + EventName, true);
						ply.sendMessage(ChatColor.BLUE + "Created " + EventName + " ,this has been set to your current location/world");
						try {
							cfg.save(new File(plugin.getDataFolder(), "event.yml"));
						} catch (IOException e) {
							Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save event.yml");
							e.printStackTrace();
						}
					}
				}
			} else if(args[0].equalsIgnoreCase("enable")) {
				if(args.length < 2) {
					ply.sendMessage(ChatColor.RED + "Not enough arguments. Do" + ChatColor.GREEN + " /event [Enabled] [EventName]");
				} else {
					String EventName = args[1];
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "event.yml"));
					if(ply.hasPermission("abeomisc.event")) {
						if(cfg.contains(EventName)) {
							cfg.set(EventName + ".state", true);
							cfg.set("Eventlist." + EventName, true);
							ply.sendMessage(ChatColor.BLUE + EventName + " has been enabled");
							try {
								cfg.save(new File(plugin.getDataFolder(), "event.yml"));
							} catch (IOException e) {
								Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save event.yml");
								e.printStackTrace();
							}
						} else {
							ply.sendMessage(ChatColor.RED + "This event does not exist");
						}
					}
				}
			} else if(args[0].equalsIgnoreCase("disable")) {
				if(args.length < 2) {
					ply.sendMessage(ChatColor.RED + "Not enough arguments. Do" + ChatColor.GREEN + " /event [disable] [EventName]");
				} else {
					String EventName = args[1];
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "event.yml"));
					if(ply.hasPermission("abeomisc.event")) {
						if(cfg.contains(EventName)) {
							cfg.set(EventName + ".state", false);
							cfg.set("Eventlist." + EventName, null);
							ply.sendMessage(ChatColor.BLUE + EventName + " has been disabled");
							try {
								cfg.save(new File(plugin.getDataFolder(), "event.yml"));
							} catch (IOException e) {
								Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save event.yml");
								e.printStackTrace();
							}
						} else {
							ply.sendMessage(ChatColor.RED + "This event does not exist");
						}
					}
				}
			} else if(args[0].equalsIgnoreCase("toggleclear")) {
				if(args.length < 2) {
					ply.sendMessage(ChatColor.RED + "Not enough arguments. Do" + ChatColor.GREEN + " /event [toggleclear] [EventName]");
				} else {
					String EventName = args[1];
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "event.yml"));
					if(ply.hasPermission("abeomisc.event")) {
						if(cfg.contains(EventName)) {
							if(cfg.getBoolean(EventName + ".clear")) {
								cfg.set(EventName + ".clear", false);
								ply.sendMessage(ChatColor.BLUE + EventName + " will no longer clear inv's on teleport");
								try {
									cfg.save(new File(plugin.getDataFolder(), "event.yml"));
								} catch (IOException e) {
									Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save event.yml");
									e.printStackTrace();
								}
								return true;
							} else {
								cfg.set(EventName + ".clear", true);
								ply.sendMessage(ChatColor.BLUE + EventName + " will clear inv's on teleport");
								try {
									cfg.save(new File(plugin.getDataFolder(), "event.yml"));
								} catch (IOException e) {
									Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save event.yml");
									e.printStackTrace();
								}
								return true;
							}

						} else {
							ply.sendMessage(ChatColor.RED + "This event does not exist");
						}
					}
				}
			} else {
				String EventName = args[0];
				FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "event.yml"));

				if(!cfg.contains(EventName)) {
					ply.sendMessage(ChatColor.RED + "No such event, if this is wrong contact a moderator");
					return true;
				}

				if(cfg.getBoolean(EventName + ".state", true)) {
					Vector pos = getVector(cfg, EventName + ".pos");
					String world = cfg.getString(EventName + ".world");

					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new WarpRunnable((Player) sender, new Location(Bukkit.getWorld(world), pos.getX(), pos.getY(), pos.getZ()), EventName), 20 * 10);
					if(cfg.getBoolean(EventName + ".clear")) {
						clearInv(ply);
						sender.sendMessage(ChatColor.BLUE + "Your inventory has been cleared. Due to event rules!");
					}
					sender.sendMessage(ChatColor.BLUE + "You will be teleported to " + EventName + " in 10 seconds...");
				} else if(cfg.getBoolean(EventName + ".state", false)) {
					ply.sendMessage(ChatColor.RED + "Sorry but " + EventName + " is currently disabled");
				} else {
					ply.sendMessage(ChatColor.RED + EventName + " may currently not exist or is having problems! Please contact a moderator");
				}
			}
		}
		return false;
	}

	private void clearInv(Player ply) {
		PlayerInventory inv = ply.getInventory();
		inv.clear();
		inv.setHelmet(null);
		inv.setChestplate(null);
		inv.setLeggings(null);
		inv.setBoots(null);
		inv.clear();
	}

}
