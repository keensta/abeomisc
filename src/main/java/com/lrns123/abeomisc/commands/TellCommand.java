package com.lrns123.abeomisc.commands;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeomisc.AbeoMisc;

public class TellCommand implements CommandExecutor {
	private final AbeoMisc plugin;
	public final HashMap<String, String> replylist = new HashMap<String, String>();

	public TellCommand(AbeoMisc plugin) {
		this.plugin = plugin;

		plugin.getCommand("tell").setExecutor(this);
		plugin.getCommand("reply").setExecutor(this);
		plugin.getCommand("r").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(!(sender instanceof Player))
			return true;
		Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("tell")) {
			if(args.length > 0) {
				Player target = Bukkit.getPlayerExact(args[0]);
				if(target == null || !target.isOnline()) {
					player.sendMessage(ChatColor.RED + args[0] + " either does not exist or is not online");
					return true;
				} else if(target == player) {
					player.sendMessage(ChatColor.RED + "You cannot send messages to yourself.");
					return true;
				} else {
					StringBuilder message = new StringBuilder();
					for(int i = 1; i < args.length; i++) {
						if(i > 1)
							message.append(" ");
						message.append(args[i]);
					}
					String result = ChatColor.GREEN + sender.getName() + " whispers " + ChatColor.YELLOW + message;
					if(sender instanceof ConsoleCommandSender) {
						Bukkit.getLogger().info("Cannot send through a console");
					}
					target.sendMessage(result);
					player.sendMessage(ChatColor.GREEN + "You have whispered " + args[0] + ", message is below");
					player.sendMessage(result);
					logtoFile(result);
					replylist.put(target.getName(), player.getName());
				}
			}
		} else if(cmd.getName().equalsIgnoreCase("reply") || label.equalsIgnoreCase("r")) {
			if(args.length > 0) {
				if(replylist.containsKey(player.getName())) {
					String targetS = replylist.get(player.getName());
					Player target = Bukkit.getPlayerExact(targetS);
					if(target == null || !target.isOnline()) {
						player.sendMessage(ChatColor.RED + args[0] + " either does not exist or is not online");
						return true;
					} else if(target == player) {
						player.sendMessage(ChatColor.RED + "You cannot send messages to yourself.");
						return true;
					} else {
						StringBuilder message = new StringBuilder();
						for(String args1 : args) {
							message.append(args1);
							message.append(" ");
						}
						String result = ChatColor.GREEN + sender.getName() + " whispers " + ChatColor.YELLOW + message;
						if(sender instanceof ConsoleCommandSender) {
							Bukkit.getLogger().info("Cannot send through a console");
						}
						target.sendMessage(result);
						player.sendMessage(ChatColor.GREEN + "You have whispered " + target.getName() + ", message is below");
						player.sendMessage(result);
						
						logtoFile(result + " [TO] " + target.getName());
						replylist.put(target.getName(), player.getName());
					}
				} else {
					player.sendMessage(ChatColor.RED + "Sorry but you have to message someone first");
				}
			}
		}

		return false;
	}

	public void logtoFile(String Message) {
		try {
			File dataFolder = plugin.getDataFolder();
			if(!dataFolder.exists())
				dataFolder.mkdir();
			
			Message.replaceAll("(?i)&([0-9A-FKLMNOR])", "");

			File saveTo = new File(plugin.getDataFolder(), "TellLog.txt");

			if(!saveTo.exists())
				saveTo.createNewFile();

			FileWriter fw = new FileWriter(saveTo, true);
			PrintWriter pw = new PrintWriter(fw);
			pw.println(Message);
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
