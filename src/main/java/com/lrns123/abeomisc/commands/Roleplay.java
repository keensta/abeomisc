package com.lrns123.abeomisc.commands;

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeomisc.AbeoMisc;

public class Roleplay implements CommandExecutor {
	
	
	private Server server;
	private final AbeoMisc plugin;
	Logger log = Logger.getLogger("Minecraft");
	
	public Roleplay(AbeoMisc plugin) {
		server = plugin.getServer();
		this.plugin = plugin;

		plugin.getCommand(".").setExecutor(this);
		plugin.getCommand("rp").setExecutor(this);
	}
	
	

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player))
			return true;
		
		if(cmd.getName().equalsIgnoreCase(".")){
			if (args.length > 0) {
				StringBuilder sb = new StringBuilder();
				for (String arg : args) {
					sb.append(arg);
					sb.append(" ");
				}
				server.broadcastMessage(ChatColor.RED + "" + sb.toString() + ChatColor.YELLOW + "{" + sender.getName() + "}");
				log.info(sender.getName() + ": " + sb.toString());
				}
		}else if(cmd.getName().equalsIgnoreCase("rp")){
			if(args.length > 0){
				StringBuilder sb = new StringBuilder();
				for (String arg : args) {
					sb.append(arg);
					sb.append(" ");
			}
				String prefix = "";
				
				if (plugin.getChat() != null) {
					prefix = plugin.getChat().getPlayerPrefix((Player) sender).replaceAll("(?i)&([0-9A-FKLMNOR])", "\u00A7$1");
					
				}
				
			server.broadcastMessage(ChatColor.WHITE + "<" + ChatColor.RESET + prefix + ((Player) sender).getDisplayName() + ChatColor.WHITE + ">" + ChatColor.GOLD + " [RP] " + ChatColor.WHITE + sb.toString());
			}
		}
		
		return false;
	}

	
	
	
}
