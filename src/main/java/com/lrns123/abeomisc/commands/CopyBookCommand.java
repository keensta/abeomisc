package com.lrns123.abeomisc.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import com.lrns123.abeomisc.AbeoMisc;

public class CopyBookCommand implements CommandExecutor {

	@SuppressWarnings("unused")
	private final AbeoMisc plugin;

	public CopyBookCommand(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getCommand("copybook").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(!(sender instanceof Player))
			return true;

		Player p = (Player) sender;

		if(p.hasPermission("abeomisc.copybook")) {
			ItemStack item = p.getItemInHand();

			if(item.getType() != Material.WRITTEN_BOOK) {
				p.sendMessage(ChatColor.RED + "You need to have a Written Book in your hand.");
				return true;
			} else {
				BookMeta oldBook = (BookMeta) item.getItemMeta();
				ItemStack book = new ItemStack(Material.WRITTEN_BOOK, 1);
				BookMeta newBook = (BookMeta) book.getItemMeta();
				newBook.setAuthor(oldBook.getAuthor());
				newBook.setTitle(oldBook.getTitle());
				newBook.setPages(oldBook.getPages());
				book.setItemMeta(newBook);
				p.getInventory().addItem(book);
				return true;
			}

		} else {
			p.sendMessage(ChatColor.RED + "Insufficent permission!");
		}

		return false;
	}

}
