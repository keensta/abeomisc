package com.lrns123.abeomisc.commands;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import com.lrns123.abeomisc.AbeoMisc;

public class HorseCommand implements CommandExecutor {

	public HashMap<String, String> players = new HashMap<String, String>();
	public HashMap<String, String> modifyOwner = new HashMap<String, String>();
	public HashMap<String, String> modifyRider = new HashMap<String, String>();

	public HorseCommand(AbeoMisc plugin) {
		plugin.getCommand("horse").setExecutor(this);
	}

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        
        if(cmd.getLabel().equalsIgnoreCase("horse")) {
            
            if(args.length == 0) {
                if(sender.isOp()) {
                    sender.sendMessage(ChatColor.RED + "Incorrect Syntax: '/horse [info/modify/transfer/rider] [add/remove] [username]");
                    return true;
                }
                sender.sendMessage(ChatColor.RED + "Incorrect Syntax: '/horse [transfer/rider] [add/remove] [username]");
                return true;
            }
            
            if(args[0].equalsIgnoreCase("info") && sender.hasPermission("abeomisc.horse.info")) {
                if(players.containsKey(sender.getName())) {
                	sender.sendMessage(ChatColor.GREEN + "Horse info disabled.");
                	players.remove(sender.getName());
                } else {
                	sender.sendMessage(ChatColor.GREEN + "Horse info enabled. Right click a horse to display info about it.");
                	players.put(sender.getName(), "info");
                }
            }
            
            if(args[0].equalsIgnoreCase("modify") && sender.hasPermission("abeomisc.horse.modify")) {
                if(args.length < 2) {
                    sender.sendMessage(ChatColor.RED + "Incorrect Syntax: '/horse modify owner [username]'");
                    return true;
                }
                
                if(args[1].equalsIgnoreCase("owner")) {
                	 OfflinePlayer p = Bukkit.getOfflinePlayer(args[2]);
                     
                     modifyOwner.put(sender.getName(), p.getName());
                     sender.sendMessage(ChatColor.GREEN + "Right click a horse to set the new owner");
                     return true;
                }
                   
                
            }
            
            if(args[0].equalsIgnoreCase("addRider")) {
            	if(args.length < 2) {
            		sender.sendMessage(ChatColor.RED + "Incorrect Syntax: '/horse addRider [username]'");
            		return true;
            	}
            	
            	OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);
            	
            	modifyRider.put(sender.getName(), p.getName());
            	sender.sendMessage(ChatColor.GREEN  + "Right click the horse to add the rider.");
            }
            
            if(args[0].equalsIgnoreCase("removeRider")) {
            	if(args.length < 2) {
            		sender.sendMessage(ChatColor.RED + "Incorrect Syntax: '/horse removeRider [username]'");
            		return true;
            	}
            	
            	OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);
            	
            	modifyRider.put(sender.getName(),  p.getName());
            	sender.sendMessage(ChatColor.GREEN + "Right click the horse to remove the rider.");
            }
            
            if(args[0].equalsIgnoreCase("transfer")) {
            	if(args.length < 2) {
            		sender.sendMessage(ChatColor.RED + "Incorrect Syntax: '/horse transfer [username]'");
            		return true;
            	}
            	
            	OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);
            	
            	modifyOwner.put(sender.getName(), op.getName());
            	sender.sendMessage(ChatColor.GREEN + "Right Click the horse to transfer the ownership.");
            }
            
        }
        
        return false;
    }
}
