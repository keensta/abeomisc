package com.lrns123.abeomisc.commands;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.lrns123.abeomisc.AbeoMisc;

public class Anniversary implements CommandExecutor{
	
	private AbeoMisc plugin;
	
	public Anniversary(AbeoMisc plugin){
		this.plugin = plugin;
		
		plugin.getCommand("abeocraft").setExecutor(this);
        plugin.getCommand("package").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		final Player ply = (Player)sender;
		
		if(cmd.getName().equalsIgnoreCase("abeocraft")){
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "anniversary.yml"));
			long lastUse = cfg.getLong(ply.getName() + ".lastUse", 0);
			
			if((System.currentTimeMillis() / 1000) - lastUse > 7200){
			cfg.set(ply.getName() + ".lastUse", (System.currentTimeMillis() / 1000));
			Bukkit.broadcastMessage(ChatColor.YELLOW + ply.getName() + " throws a cake in the sky as they celebrate one year of Abeocraft!");
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				
				@Override
				public void run() {
					Bukkit.broadcastMessage(ChatColor.GRAY + ply.getName() + " catches the cake");
					ply.getInventory().addItem(new ItemStack(Material.CAKE, 1));
				}
			}, 20 * 3);
			
			try {
				cfg.save(new File(plugin.getDataFolder(), "anniversary.yml"));
			} catch (IOException e) {
				Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save anniversary.yml");
				e.printStackTrace();
				}
			
			} else {
				long timeRemaining = (7200 - ((System.currentTimeMillis() / 1000) - lastUse)) / 60;
				sender.sendMessage(ChatColor.RED + "You can only use this once per 2 hours.");
				sender.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining.");
			}
			
		} else if(cmd.getName().equalsIgnoreCase("package")){
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "anniversary.yml"));
			Boolean used = cfg.getBoolean(ply.getName() + ".package");
			if(used == true){
				ply.sendMessage(ChatColor.RED + "Looks like you have opened your package already :)");
				return false;
			}
			Random rn = new Random();
			int number = rn.nextInt(5)+1;
			int a1 = rn.nextInt(5)+1;
			int a2 = rn.nextInt(3)+1;
			Bukkit.broadcastMessage(ChatColor.GREEN + ply.getName() + " opens up their one year anniversary package! Ooo.. what's inside?");
			cfg.set(ply.getName() + ".package", true);
			try {
				cfg.save(new File(plugin.getDataFolder(), "anniversary.yml"));
			} catch (IOException e) {
				Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save anniversary.yml");
				e.printStackTrace();
				}
			switch (number){
				case 0:
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND, a1));
					ply.getInventory().addItem(new ItemStack(Material.APPLE, a1));
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD, a2));
					ply.getInventory().addItem(new ItemStack(Material.GOLD_PICKAXE, a2));
					
					break;
				case 1:
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND, a1));
					ply.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, a1));
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND_PICKAXE, a2));
					ply.getInventory().addItem(new ItemStack(Material.APPLE, a2));
					break;
				case 2:
					ply.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, a1));
					ply.getInventory().addItem(new ItemStack(Material.APPLE, a1));
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD, a2));
					ply.getInventory().addItem(new ItemStack(Material.GOLD_PICKAXE, a2));
					break;
				case 3:
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND, a2));
					ply.getInventory().addItem(new ItemStack(Material.APPLE, a1));
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD, a2));
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND_PICKAXE, a2));
					break;
				case 4:
					ply.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, a1));
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND_PICKAXE, a2));
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD, a2));
					ply.getInventory().addItem(new ItemStack(Material.GOLD_PICKAXE, a2));
					break;
				case 5:
					ply.getInventory().addItem(new ItemStack(Material.DIAMOND, a1));
					ply.getInventory().addItem(new ItemStack(Material.APPLE, a1));
					ply.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, a1));
					ply.getInventory().addItem(new ItemStack(Material.GOLD_PICKAXE, 1));
					break;
			}
			
		}
		
		return false;
	}
	
	

}
