package com.lrns123.abeomisc.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.lrns123.abeomisc.AbeoMisc;
import com.lrns123.abeomisc.runnables.WarpRunnable;

public class WarpCommand implements CommandExecutor {

	private AbeoMisc plugin;

	public WarpCommand(AbeoMisc plugin) {
		this.plugin = plugin;
		plugin.getCommand("warp").setExecutor(this);
	}

	private Vector getVector(FileConfiguration cfg, String path) {
		try {
			List<Integer> list = cfg.getIntegerList(path);
			if(list == null || list.size() != 3)
				return new Vector();

			return new Vector(list.get(0), list.get(1), list.get(2));
		} catch (Exception e) {
			return new Vector();
		}
	}

	private void setVector(FileConfiguration cfg, String path, Vector vec) {

		List<Integer> list = new ArrayList<Integer>();
		list.add(vec.getBlockX());
		list.add(vec.getBlockY());
		list.add(vec.getBlockZ());

		cfg.set(path, list.toArray(new Integer[0]));

	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player))
			return true;

		if(args.length == 0)
			return true;
		
		if(!sender.hasPermission("abeomisc.warp")) {
		    sender.sendMessage(ChatColor.RED + "Insufficient permissions");
		    return true;
		}

		int length = 10800;
		int wait = 3;

		if(args[0].equalsIgnoreCase("spawn")) {

			FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));

			if(!cfg.contains("spawnpos") || !cfg.contains("spawnworld")) {
				sender.sendMessage(ChatColor.RED + "No spawn is set! Please contact a moderator.");
				return true;
			}

			long lastTp = cfg.getLong(sender.getName() + ".lastwarp", 0);
			if((System.currentTimeMillis() / 1000) - lastTp > length) {
				Vector pos = getVector(cfg, "spawnpos");
				String world = cfg.getString("spawnworld");

				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new WarpRunnable((Player) sender, new Location(Bukkit.getWorld(world), pos.getX(), pos.getY(), pos.getZ()), "spawn"), 20 * 10);
				sender.sendMessage(ChatColor.BLUE + "You will be teleported to spawn in 10 seconds...");

				cfg.set(sender.getName() + ".lastwarp", (System.currentTimeMillis() / 1000));
				try {
					cfg.save(new File(plugin.getDataFolder(), "warp.yml"));
				} catch (IOException e) {
					Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save warp.yml!");
					e.printStackTrace();
				}
			} else {
				long timeRemaining = (length - ((System.currentTimeMillis() / 1000) - lastTp)) / 60;
				sender.sendMessage(ChatColor.RED + "You can only use warp once per " + wait + " hours.");
				sender.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining.");
			}
		} else if(args[0].equalsIgnoreCase("bed") && sender.hasPermission("abeomisc.premium")) {
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));

			Player ply = (Player) sender;
			Location bedloc = ply.getBedSpawnLocation();
			if(bedloc == null) {
				sender.sendMessage(ChatColor.RED + "You bed is not set or obstructed.");
				return true;
			}

			// HOTFIX: Raise Y coordinate by 1.5 to avoid warping into the
			// floor, Now added 1 to the X
			bedloc.add(1.5, 1.5, 0);

			long lastTp = cfg.getLong(sender.getName() + ".lastwarp", 0);
			if((System.currentTimeMillis() / 1000) - lastTp > 14400) {

				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new WarpRunnable((Player) sender, bedloc, "your bed"), 20 * 10);
				sender.sendMessage(ChatColor.BLUE + "You will be teleported to your bed in 10 seconds...");

				cfg.set(sender.getName() + ".lastwarp", (System.currentTimeMillis() / 1000));
				try {
					cfg.save(new File(plugin.getDataFolder(), "warp.yml"));
				} catch (IOException e) {
					Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save warp.yml!");
					e.printStackTrace();
				}
			} else {
				long timeRemaining = (14400 - ((System.currentTimeMillis() / 1000) - lastTp)) / 60;
				sender.sendMessage(ChatColor.RED + "You can only use warp once per " + wait + " minutes.");
				sender.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining.");
			}
		} else if(args[0].equalsIgnoreCase("setspawn")) {
			if(sender.hasPermission("abeomisc.setwarpspawn")) {
				FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));
				setVector(cfg, "spawnpos", ((Player) sender).getLocation().toVector());
				cfg.set("spawnworld", ((Player) sender).getLocation().getWorld().getName());
				sender.sendMessage(ChatColor.GREEN + "Spawn warp location has been set!");
				try {
					cfg.save(new File(plugin.getDataFolder(), "warp.yml"));
				} catch (IOException e) {
					Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save warp.yml!");
					e.printStackTrace();
				}
			}
		} else if(args[0].equalsIgnoreCase("direct")) {
			if(sender.hasPermission("abeomisc.warpdirect")) {
				if(args.length < 2) {
					sender.sendMessage(ChatColor.RED + "Please specify a destination");
				} else {
					String destination = args[1].toLowerCase();
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));
					if(cfg.contains("destinations." + destination)) {
						Vector pos = getVector(cfg, "destinations." + destination + ".pos");
						String world = cfg.getString("destinations." + destination + ".world");

						Player ply = (Player) sender;
						ply.sendMessage(ChatColor.BLUE + "Warping to " + destination);
						ply.teleport(new Location(Bukkit.getWorld(world), pos.getX(), pos.getY(), pos.getZ()));
					} else {
						sender.sendMessage(ChatColor.RED + "Unknown destination");
					}
				}
			}
		} else if(args[0].equalsIgnoreCase("directlist")) {
			if(sender.hasPermission("abeomisc.warpdirect")) {

				FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));
				if(cfg.contains("destinations")) {

					Set<String> destinations = cfg.getConfigurationSection("destinations").getKeys(false);

					StringBuilder sb = new StringBuilder();
					for(String dest : destinations) {
						sb.append(dest);
						sb.append(" ");
					}
					sender.sendMessage(ChatColor.BLUE + "Available direct warp destinations:");
					sender.sendMessage(ChatColor.BLUE + sb.toString());
				} else {
					sender.sendMessage(ChatColor.RED + "No direct destinations are set");
				}
			}
		} else if(args[0].equalsIgnoreCase("setdirect")) {
			if(sender.hasPermission("abeomisc.setwarpdirect")) {
				if(args.length < 2) {
					sender.sendMessage(ChatColor.RED + "Please specify a destination");
				} else {

					String destination = args[1].toLowerCase();
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));

					setVector(cfg, "destinations." + destination + ".pos", ((Player) sender).getLocation().toVector());
					cfg.set("destinations." + destination + ".world", ((Player) sender).getLocation().getWorld().getName());

					sender.sendMessage(ChatColor.BLUE + "Added direct warp destination " + destination);

					try {
						cfg.save(new File(plugin.getDataFolder(), "warp.yml"));
					} catch (IOException e) {
						Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save warp.yml!");
						e.printStackTrace();
					}
				}
			}
		} else if(args[0].equalsIgnoreCase("removedirect")) {
			if(sender.hasPermission("abeomisc.setwarpdirect")) {
				if(args.length < 2) {
					sender.sendMessage(ChatColor.RED + "Please specify a destination");
				} else {

					String destination = args[1].toLowerCase();
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warp.yml"));

					cfg.set("destinations." + destination, null);

					sender.sendMessage(ChatColor.BLUE + "Removed direct warp destination " + destination);

					try {
						cfg.save(new File(plugin.getDataFolder(), "warp.yml"));
					} catch (IOException e) {
						Logger.getLogger("Minecraft").severe("[AbeoMisc] Could not save warp.yml!");
						e.printStackTrace();
					}
				}
			}
		}

		return true;
	}

}
