package com.lrns123.abeomisc.commands;

import java.io.File;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeomisc.AbeoMisc;

public class NamePetCommand implements CommandExecutor{
	
	@SuppressWarnings("unused")
	private final AbeoMisc plugin;
	public HashMap<String, String> petNames = new HashMap<String, String>();
	
	public NamePetCommand(AbeoMisc plugin){
		this.plugin = plugin;
		plugin.getCommand("namepet").setExecutor(this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getLabel().equalsIgnoreCase("namepet")) {
			if((sender instanceof Player) && sender.hasPermission("abeomisc.namepet")) {
				Player ply = (Player) sender;
				if(args.length == 1) {
					File p1 = new File("world/players", args[0] + ".dat");
					if(!p1.exists()) {
						petNames.put(ply.getName(), args[0]);
						ply.sendMessage(ChatColor.GREEN + "Right-Click your pet to set their name to " + ChatColor.GRAY + args[0]);
					} else {
						ply.sendMessage(ChatColor.RED + "You cannot name your pet after an existing player.");
					}
				} else {
					ply.sendMessage(ChatColor.BLUE + "Usage: /namepet <name>");
				}
			}
		}
		
		return false;
	}
	
}
