package com.lrns123.abeomisc.recipes;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class RecipesAddOn {

	@SuppressWarnings("deprecation")
	public RecipesAddOn() {
		// Add wonderful recipes. Yum!
		Bukkit.addRecipe(new ShapedRecipe(new ItemStack(43, 1, (short) 8)).shape("SS").setIngredient('S', Material.STEP)); //'Special Slab Block
		Bukkit.addRecipe(new ShapedRecipe(new ItemStack(48, 1)).shape("VVV","VBV","VVV").setIngredient('B', Material.COBBLESTONE).setIngredient('V', Material.VINE)); // Mossy Cobble
		Bukkit.addRecipe(new ShapedRecipe(new ItemStack(98, 1, (short) 1)).shape("VVV","VBV","VVV").setIngredient('B', Material.SMOOTH_BRICK).setIngredient('V', Material.VINE)); // Mossy Brick
		Bukkit.addRecipe(new ShapedRecipe(new ItemStack(98, 1, (short) 2)).shape("FBF","BFB","FBF").setIngredient('B', Material.SMOOTH_BRICK).setIngredient('F', Material.FLINT)); // Cracked Brick
		}
}
